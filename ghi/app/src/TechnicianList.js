import React, { useEffect, useState } from "react";


function TechnicianList(props) {
    const [technicians, setTechnicians] = useState([]);

    async function loadTechnicians() {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
        const data = await response.json();
        setTechnicians(data);
    }}

    useEffect(() => {
        loadTechnicians();
    }, []);

    if (!technicians) {
        return <div>No technicians available</div>
    }

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee Id</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.technicians?.map(technicians => {
                    return (
                        <tr key={technicians.employee_id}>
                            <td>{ technicians.first_name }</td>
                            <td>{ technicians.last_name }</td>
                            <td>{ technicians.employee_id }</td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default TechnicianList;
