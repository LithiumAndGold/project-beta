import React, { useEffect, useState } from "react";

function CustomersList() {
  const [customers, setCustomers] = useState([]);

  async function loadCustomers() {
    const response = await fetch("http://localhost:8090/api/customers/")
    if (response.ok) {
      const data = await response.json();
      setCustomers(data);
    }
  }

  useEffect(() => {
    loadCustomers();
  }, [])

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Address</th>
            <th>Phone number</th>
          </tr>
        </thead>
        <tbody>
          {customers.customers?.map(customer => {
            return (
              <tr key={customer.id}>
                <td>{customer.last_name}</td>
                <td>{customer.first_name}</td>
                <td>{customer.address}</td>
                <td>{customer.phone_number}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default CustomersList;
