import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import CustomerForm from "./CustomerForm";
import CustomersList from "./CustomersList";
import SaleForm from "./SaleForm";
import SalesList from "./SalesList";
import SalepersonForm from "./SalespersonForm";
import SalespeopleList from "./SalespeopleList";
import AutomobileForm from "./AutomobileForm";
import AutomobilesList from "./AutomobilesList";
import ManufacturerForm from "./ManufacturerForm";
import ManufacturersList from "./ManufacturersList";
import VehicleModelForm from "./VehicleModelForm";
import VehicleModelsList from "./VehicleModelsList";
import TechnicianForm from "./TechnicianForm";
import TechnicianList from "./TechnicianList";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import SalespersonHistory from "./SalespersonHistory";
import ServiceHistory from "./ServiceHistory";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales/">
            <Route index element={<SalesList />} />
            <Route path="new/" element={<SaleForm />} />
          </Route>
          <Route path="salespeople/">
            <Route index element={<SalespeopleList />} />
            <Route path="new/" element={<SalepersonForm />} />
            <Route path="history/" element={<SalespersonHistory />} />
          </Route>
          <Route path="customers/">
            <Route index element={<CustomersList/>} />
            <Route path="new/" element={<CustomerForm />} />
          </Route>
          <Route path="models/">
            <Route index element={<VehicleModelsList />} />
            <Route path="new/" element={<VehicleModelForm />} />
          </Route>
          <Route path="manufacturers/">
            <Route index element={<ManufacturersList />} />
            <Route path="new/" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles/">
            <Route index element={<AutomobilesList />} />
           <Route path="new/" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians/">
            <Route index element={<TechnicianList technicians={props.technicians} />} />
              <Route path="new/" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route index element={<AppointmentList appointments={props.appointments} />} />
              <Route path="new/" element={<AppointmentForm />} />
          </Route>
          <Route path="service/">
            <Route path="history/" element={<ServiceHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
