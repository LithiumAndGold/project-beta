import React, { useEffect, useState } from "react";

function SalesList() {
  const [sales, setSales] = useState([]);

  async function loadSales() {
    const response = await fetch("http://localhost:8090/api/sales/")
    if (response.ok) {
      const data = await response.json();
      setSales(data);
    }
  }

  useEffect(() => {
    loadSales();
  }, [])

  let key = 1

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Employee ID</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.sales?.map(sale => {
            return (
              <tr key={ key ++ }>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default SalesList;
