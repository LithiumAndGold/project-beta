import React, { useState, useEffect } from "react";


function TechnicianForm() {
    const [technicians, setTechnicians] = useState([]);
    const [first_name, setFirstName] = useState("");
    const [last_name, setLastName] = useState("");
    const [employee_id, setEmployeeId] = useState("");
    const [technician, setTechnician] = useState("");

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;
        data.technician = `/api/technicians/${technician}`;

        const technicianUrl = "http://localhost:8080/api/technicians/";
        const fetchOptions = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const technicianResponse = await fetch(technicianUrl, fetchOptions);
        if (technicianResponse.ok) {
            setFirstName("");
            setLastName("");
            setEmployeeId("");
            setTechnician("");
        }
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} placeholder="FirstName" required type="text" name="first_name" id="first_name" className="form-control" value={first_name}/>
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} placeholder="LastName" required type="text" name="last_name" id="last_name" className="form-control" value={last_name}/>
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} placeholder="EmployeeId" required type="text" name="employee_id" id="employee_id" className="form-control" value={employee_id}/>
                            <label htmlFor="employee_id">Employee Id</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
