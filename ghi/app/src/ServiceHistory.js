import React, { useState, useEffect } from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState("");
    const [filterAppointments, setFilterAppointments] = useState([]);


    async function fetchAppointments() {
        try {
            const response = await fetch("http://localhost:8080/api/appointments/");
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
                setFilterAppointments(data.appointments);
            } else {
                console.error("Fetching appointments failed");
            }
        } catch (error) {
            console.error("Fetching appointments failed:", error);
        }
    }

    function handleSearch(event) {
        event.preventDefault();
        const query = search.trim().toLowerCase();
        if (query === "") {
            setFilterAppointments(appointments);
        } else {
            const filter = appointments.filter(appointment =>
                appointment.vin.toLowerCase() === query
            );
            setFilterAppointments(filter);
        }
    }

    const handleInput = (event) => {
        const value = event.target.value;
        setSearch(value);
    }

    useEffect(() => {
        fetchAppointments();
    }, []);

    return (
        <div>
            <h1>Service History</h1>
            <form onSubmit={handleSearch}>
                <input onChange={handleInput} type="text" placeholder="Search by VIN" value={search} />
                <button className="btn btn-primary" type="submit">Search</button>
            </form>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Automobile VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filterAppointments.map(appointment => (
                        <tr key={appointment.vin}>
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.vip.toString() }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ appointment.date }</td>
                            <td>{ appointment.time }</td>
                            <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory;
