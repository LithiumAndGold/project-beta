import React, { useEffect, useState } from "react";

function AppointmentList(props) {
    const [appointments, setAppointments] = useState([]);

    async function loadAppointments() {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (response.ok) {
            const data = await response.json();
            const loadAppointments = data.appointments.filter(appointment =>
                appointment.status !== "canceled" && appointment.status !== "finished");

            const salesResponse = await fetch("http://localhost:8090/api/sales");
            if (salesResponse.ok) {
                const salesData = await salesResponse.json();
                const sales = salesData.sales;

                const updateAppointments = loadAppointments.map((appointment) => {
                    const matchSale = sales.find((sale) => sale.automobile.vin === appointment.vin);
                    if (matchSale) {
                        return {...appointment, vip: true};
                    }
                    return appointment;
                });
                setAppointments(updateAppointments);
            } else {
                console.error("Failed to fetch sales data");
            }
        } else {
            console.error("Failed to fetch appointments data");
        }
    };

    useEffect(() => {
        loadAppointments();
    }, []);

    if (!appointments) {
        return <div>No appointments in database</div>
    }


    async function cancelAppointment(appointmentId) {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/cancel`,
            {method: "PUT"});
            if (response.ok) {
                const updateAppointments = appointments.map(appointment => {
                    if (appointment.vin === appointmentId) {
                        return {...appointment, status: "canceled"};
                    }
                    return appointment;
                });
                const filterAppointments = updateAppointments.filter(appointment => appointment.status !== "canceled");
                setAppointments(filterAppointments);
            } else {
                console.error("Cancel appointment failed");
            }
        } catch (error) {
            console.error("Cancel appoitnment failed:", error);
        }
    }

    async function finishAppointment(appointmentId) {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/finish`,
            {method: "PUT"});
            if (response.ok) {
                const updateAppointments = appointments.map(appointment => {
                    if (appointment.vin === appointmentId) {
                        return {...appointment, status: "finished"};
                    }
                    return appointment;
                });
                const filterAppointments = updateAppointments.filter(appointment => appointment.status !== "finished");
                setAppointments(filterAppointments);
            } else {
                console.error("Finish appointment failed");
            }
        } catch (error) {
            console.error("Finish appoitnment failed:", error);
        }
    }


    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Automobile VIN</th>
                        <th>VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                    return (
                        <tr key={appointment.vin}>
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.vip.toString() }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ appointment.date }</td>
                            <td>{ appointment.time }</td>
                            <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                            <td>{ appointment.reason }</td>
                            <td><button className="btn btn-primary" onClick={() => cancelAppointment(appointment.vin)}>Cancel</button>
                            <button className="btn btn-primary" onClick={() => finishAppointment(appointment.vin)}>Finish</button></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default AppointmentList;
