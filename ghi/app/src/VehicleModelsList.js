import React, { useEffect, useState } from "react";

function VehicleModelsList() {
  const [models, setModels] = useState([]);

  async function loadModels() {
  const response = await fetch("http://localhost:8100/api/models/")
  if (response.ok) {
    const data = await response.json();
    setModels(data.models);
  }
  }

  useEffect(() => {
    loadModels();
  }, [])

  let key = 1

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map(model => {
            return (
              <tr key={ key ++ }>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td><a href={model.picture_url}>{model.name}</a></td>
              </tr>
            )
          })}

        </tbody>
      </table>
    </div>
  )
}

export default VehicleModelsList;
