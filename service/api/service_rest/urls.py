from django.urls import path
from .views import api_list_appointments, api_show_appointments
from .views import api_list_technicians, api_show_technicians
from .views import api_cancel_appointment, api_finish_appointment


urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path(
        "appointments/<str:id>/cancel",
        api_cancel_appointment,
        name="api_cancel_appointment"
    ),
    path(
        "appointments/<str:id>/finish",
        api_finish_appointment,
        name="api_finish_appointment"
    ),
    path(
        "appointments/<int:id>/",
        api_show_appointments,
        name="api_show_appointments"
    ),
    path(
        "technicians/<int:id>",
        api_show_technicians,
        name="api_show_technicians"
    ),
]
