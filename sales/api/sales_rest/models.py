from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=150)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    color = models.CharField(max_length=150)
    year = models.PositiveIntegerField(null=True)
    manufacturer = models.CharField(max_length=150)
    model = models.CharField(max_length=150)

    class Meta:
        ordering = ["year"]

class Salesperson(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.first_name

    class Meta:
        ordering = ["last_name"]

class Customer(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    address = models.TextField()
    phone_number = models.CharField(max_length=15)

    def __str__(self):
        return self.first_name

    class Meta:
        ordering = ["last_name"]

class Sale(models.Model):
    price = models.IntegerField()

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salespeople",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"sale: {self.pk}"
